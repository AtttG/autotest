from word_len_finder import FindLength

def test_case_1():
    assert FindLength("cat") == 3
def test_case_2():
    assert FindLength("test") == 4
def test_case_3():
    assert FindLength("apple") == 5
def test_case_4():
    assert FindLength("kitten") == 6
def test_case_5():
    assert FindLength("building") == 8

